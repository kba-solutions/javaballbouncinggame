 

package bouncinggame;  


import bouncinggame.BouncingGame;
 
public class BouncingGame {

    
    public static void main(String[] args) { 
            Game game = new Game(Config.SCREEN_WIDTH, Config.SCREEN_HEIGHT, "Bouncing Balls");
            game.start();
    }

}
