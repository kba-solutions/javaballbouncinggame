
package bouncinggame;

import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.Color;
import java.util.*;

class Game implements Runnable {

	public int width, height, fps = 60;
	private String title;
	public Display display;

	public Thread thread;
	public boolean running = false;
	private BufferStrategy bs;
	private Graphics g;
	private List<GameObject> objects = new ArrayList<GameObject>();

	public Game(int width, int height, String title) {
		this.width = width;
		this.height = height;
		this.title = title;
		List<Ball> balls = new ArrayList<Ball>();
		List<Box> boxes = new ArrayList<Box>();
		Random rand = new Random(System.currentTimeMillis());
		for (int i = 0; i < Config.COLORS.size(); i++) {
			Box box = new Box(rand.nextFloat() * (Config.SCREEN_WIDTH - Config.BOX_WIDTH), rand.nextFloat() *( Config.SCREEN_HEIGHT - Config.BOX_HEIGHT) ,
					Config.BOX_WIDTH, Config.BOX_HEIGHT, 0, 0, Config.COLORS.get(i));
			boxes.add(box);
		}

		int ballNum = Math.abs(rand.nextInt()) % (Config.MAX_BALL_NUM - Config.MIN_BALL_NUM) + Config.MIN_BALL_NUM;

		for (int i = 0; i < ballNum; i++) {
			float ballX = rand.nextFloat() * Config.SCREEN_WIDTH;
			float ballY = rand.nextFloat() * Config.SCREEN_HEIGHT;
			float ballVx = rand.nextFloat() * (Config.MAX_OBJECT_SPEED - Config.MIN_OBJECT_SPEED)
					+ Config.MIN_OBJECT_SPEED;
			float ballVy = rand.nextFloat() * (Config.MAX_OBJECT_SPEED - Config.MIN_OBJECT_SPEED)
					+ Config.MIN_OBJECT_SPEED;
			Color ballColor = Config.COLORS.get(Math.abs(rand.nextInt()) % Config.COLORS.size());
			Ball ball = new Ball(ballX, ballY, Config.BALL_DIAMETER, ballVx, ballVy, ballColor);
			balls.add(ball);
		}
		objects.addAll(balls);
		objects.addAll(boxes);
	}

	private void init() {
		display = new Display(width, height, title);
	}

	private void tick(double dt) {
		// update variables here
		for (int i = 0; i < objects.size(); i++) {
			objects.get(i).Update(dt, objects);
		}
	}

	private void render() {
		bs = display.canvas.getBufferStrategy();
		if (bs == null) {
			display.canvas.createBufferStrategy(3);
			return;
		}
		//
		g = bs.getDrawGraphics();
		g.clearRect(0, 0, width, height);
		g.setColor(Config.BACKGROUND_COLOR);
		g.fillRect(0, 0, width, height);
		// draw here
		for (int i = 0; i < objects.size(); i++) {
			objects.get(i).Render(g);
		}

		bs.show();
		g.dispose();
	}

	public void run() {
		init();

		double tickDuration = 1000000000 / fps;
		double delta = 0;
		long now;
		long lastTime = System.nanoTime();

		while (running) {
			now = System.nanoTime();
			delta += (now - lastTime) / tickDuration;
			lastTime = now;

			if (delta >= 1) {
				tick(delta);
				render();
				delta--;
			}
		}

		stop();
	}

	public synchronized void start() {
		if (running)
			return;
		running = true;
		thread = new Thread(this);
		thread.start();
	}

	public synchronized void stop() {
		if (!running)
			return;
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}