package bouncinggame;

import bouncinggame.GameObject;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.List;

public class Box extends GameObject {

    private int ballCount = 0;

    Box(float x, float y, float width, float height, float vx, float vy, Color color) {
        super(x, y, width, height, vx, vy, color);
    }

    public void Render(Graphics g) {
        if (!isEnabled)
            return;
        super.Render(g);
        g.fillRect((int) x, (int) y, (int) width, (int) height);

        Font font = new Font("Serif", Font.PLAIN, 20);
        g.setFont(font);
        g.setColor(Config.TEXT_COLOR);
        g.drawString(Integer.toString(ballCount), (int) (x + width / 3), (int) (y + height / 2));
    }

    public void Update(double dt, List<GameObject> objects) {
        super.Update(dt, objects);
        for (int i = 0; i < objects.size(); i++) {
            GameObject obj = objects.get(i);
            if (obj.isEnabled && AABBWithObj(obj) && obj instanceof Ball && this.color == obj.color) {
                ballCount++;
                obj.isEnabled = false;
            }
        }
    }
}