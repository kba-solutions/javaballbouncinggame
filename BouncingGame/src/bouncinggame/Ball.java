package bouncinggame;

import java.awt.Graphics;
import java.awt.Color;
import bouncinggame.GameObject;

class Ball extends GameObject {
    public Ball(float x, float y, float diameter, float vx, float vy, Color color) {
        super(x, y, diameter, diameter, vx, vy, color);
    }

    public void drawCircle(Graphics g, int x, int y, int radius) {
        int diameter = radius * 2;
        g.fillOval(x - radius, y - radius, diameter, diameter);
    }

    public void Render(Graphics g) {
        if (!isEnabled)
            return;
        super.Render(g);
        drawCircle(g, (int) x, (int) y, (int) (width / 2));
    }
}