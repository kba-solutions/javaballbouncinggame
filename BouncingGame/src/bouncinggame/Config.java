package bouncinggame;

import java.util.ArrayList;
import java.awt.Color;

class Config {
    public static int SCREEN_WIDTH = 900;
    public static int SCREEN_HEIGHT = 900;
    public static float MAX_OBJECT_SPEED = 2f;
    public static float MIN_OBJECT_SPEED = 0.5f;
    public static float BALL_DIAMETER = 20;
    public static float BOX_WIDTH = 50;
    public static float BOX_HEIGHT = 70;
    public static int MIN_BALL_NUM = 30;
    public static int MAX_BALL_NUM = 50;
    public static Color BACKGROUND_COLOR = Color.PINK;
    public static Color TEXT_COLOR = Color.PINK;

    public static ArrayList<Color> COLORS = new ArrayList<Color>() {
        {
            add(Color.RED);
            add(Color.GREEN);
            add(Color.YELLOW);
            add(Color.BLACK);
            add(Color.WHITE);
            add(Color.BLUE);
        }
    };
}