
package bouncinggame;

import bouncinggame.Config;

import java.awt.Graphics;
import java.lang.Math;
import java.awt.Color;

import java.util.*;

class GameObject {
    protected float x;
    protected float y;
    protected float vx;
    protected float vy;
    protected Color color;
    protected float width;
    protected float height;
    protected boolean isEnabled = true;

    public GameObject(float x, float y, float width, float height, float vx, float vy, Color color) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.vx = vx;
        this.vy = vy;
        this.color = color;
    }

    public boolean AABBWithObj(GameObject obj) {
        if (this.x + this.width > obj.x && this.x < obj.x + obj.width && this.y + this.height > obj.y
                && this.y < obj.y + obj.height) {
                    return true;
        }
        return false;
    }

    public void Update(double dt, List<GameObject> objects) {
        if (!isEnabled)
            return;
        this.x += this.vx * dt;
        this.y += this.vy * dt;
        if (this.x + this.width / 2 > Config.SCREEN_WIDTH) {
            this.x = Config.SCREEN_WIDTH - this.width / 2;
            this.vx = -Math.abs(this.vx);
        } else if (this.x - this.width / 2 < 0) {
            this.x = this.width / 2;
            this.vx = Math.abs(this.vx);
        }
        if (this.y + this.height / 2 > Config.SCREEN_HEIGHT) {
            this.y = Config.SCREEN_HEIGHT - this.height / 2;
            this.vy = -Math.abs(this.vy);
        } else if (this.y - this.height / 2 < 0) {
            this.y = this.height / 2;
            this.vy = Math.abs(this.vy);
        }
    }

    public void Render(Graphics g) {
        g.setColor(color);
    }

}